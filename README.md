# Instructions for getting the VM up and running

1. Download and install both [Vagrant](http://www.vagrantup.com/downloads.html) and [VirtualBox](https://www.virtualbox.org).

2. Download this repository, save it in a place you will remember and install the VM. Open up terminal or command line and perform the following

        cd /path/to/Sparkstone_Vagrant
        vagrant up

3. Wait for `vagrant up` to finish. This could take up to 30 minutes depending on your internet connection and computer speed. **Do not shutdown/close your laptop or interact with the virtualbox window that opens**.

4. Once the `vagrant up` command has finished, run `vagrant halt`. This will shutdown the virtual machine and allow the software updates to take place.

4. Now, run `vagrant up` once more, and an ubuntu GUI should open.

5. Now type the command `bash <(curl -s http://sparkstone.elasticbeanstalk.com/downloads/file/vm_init.sh)`. This will install any neccessary software for sparkstone and could take up to an hour. Near the end of the script, you will be prompted for information in order to finalize the setup.

6. Next run `source ~/.bashrc` to apply all your environment changes. You are all setup after this.
